function createTableByNumber() {
  var placeholder = document.getElementsByClassName('container')[0];

  var table = document.createElement('table');
  var thead = document.createElement('thead');
  var tbody = document.createElement('tbody');

  var number = document.getElementById('number').value;

  table.setAttribute('class', 'table');

  var rowHead = document.createElement('tr');
  var head = document.createElement('th');
  head.setAttribute('scope', 'col');
  head.innerHTML = "#";
  rowHead.appendChild(head);
  thead.appendChild(rowHead);

  if (number % 1 != 0) {
    return;
  }

  for (var i = 0; i < number; i++) {
    var bodyRow = document.createElement('tr');
    var rowNum = document.createElement('td');
    rowNum.innerHTML = i + 1;
    bodyRow.appendChild(rowNum);
    tbody.appendChild(bodyRow);
  }

  table.appendChild(thead);
  table.appendChild(tbody);
  placeholder.appendChild(table);
}