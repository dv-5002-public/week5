"use strict";

function showAlert(text) {
    console.log(text);
}

function listFormElement() {
    var formElements = document.getElementsByTagName('form');
    console.log(formElements);
}

function copyTextFromNameToLastName() {
    var nameElement = document.getElementById('firstName');
    var surnameElement = document.getElementById('lastName');
    var firstName = nameElement.value;
    console.log(`firstname is ${firstName}`);
    console.log(`now lastname is ${surnameElement.value}`);
    surnameElement.value = firstName;
}

function Reset() {
    let elements = document.getElementsByTagName('form');
    for (let i = 0; i < elements[0].length; i++) {
        if (elements[0][i].getAttribute('type') == 'radio') {
            elements[0][i].checked = false;
        } else {
            elements[0][i].value = null;
        }
    }

    hideSummary();
}

function copyRadioToEmail() {
    let radioElements = document.getElementById('studentStatus')
        .getElementsByTagName('input');
    for (let i = 0; i < radioElements.length; i++) {
        if (radioElements[i].checked) {
            document.getElementById('email').value =
                radioElements[i].value;
        }
    }
}

function showSummary() {
    let summaryElement = document.getElementById('summary');
    summaryElement.removeAttribute('hidden')

    if (summaryElement.innerHTML != "") {
        summaryElement.innerHTML = "";
    }

    let name = document.getElementById('firstName').value;
    summaryElement.innerHTML += `<p><b>First name</b>: ${name} <\p>`;

    let lastName = document.getElementById('lastName').value;
    summaryElement.innerHTML += `<p><b>Last name</b>: ${lastName} <\p>`;

    let email = document.getElementById('email').value;
    summaryElement.innerHTML += `<p><b>Email</b>: ${email} <\p>`;

    let password = document.getElementById('password').value;
    summaryElement.innerHTML += `<p><b>Password</b>: ${password} <\p>`;

    let studentStatus = getStatus();
    summaryElement.innerHTML += `<p><b>Student status</b>: ${studentStatus} <\p>`;
}

function getStatus() {
    let radioElements = document.getElementById('studentStatus')
        .getElementsByTagName('input');
    for (let i = 0; i < radioElements.length; i++) {
        if (radioElements[i].checked) {
            return radioElements[i].value;
        }
    }
}

function hideSummary() {
    let summaryElement = document.getElementById('summary');
    summaryElement.setAttribute('Hidden', 'true');
}

function isEmpty(id) {
    if (document.getElementById(id).value == "") {
        hideSummary();
    }
}

function addPElement() {
    var node = document.createElement('p')
    node.innerHTML = 'Hello world'
    var placeholder = document.getElementById('placeholder')
    placeholder.appendChild(node)
}

function addHref() {
    var node = document.createElement('a')
    node.innerHTML = 'google'
    node.setAttribute('href', 'https://www.google.com')
    node.setAttribute('target', '_blank')
    var placeholder = document.getElementById('placeholder')
    placeholder.appendChild(node)
}

function addBoth() {
    var node = document.createElement('p')
    node.innerHTML = 'google'
    var node2 = document.createElement('a')
    node2.innerHTML = 'google'
    node2.setAttribute('href', 'https://www.google.com')
    node2.setAttribute('target', '_blank')
    var placeholder = document.getElementById('placeholder')
    placeholder.appendChild(node)
    placeholder.appendChild(node2)
}

function createForm() {
    var nameCol = document.createElement('div');
    var lastNameCol = document.createElement('div');
    var emailCol = document.createElement('div');
    var form = document.createElement('form');
    let formGroup = document.createElement('div');
    let formGroup2 = document.createElement('div');
    let formGroup3 = document.createElement('div');
    var row = document.createElement('div');
    var headerText = document.createElement('p');
    var placeHolder = document.getElementsByClassName('container')[0]
    let colSeperate = document.createElement('div');
    colSeperate.setAttribute('class', 'w-100');
    headerText.innerHTML = 'Please fill this form to create account';
    placeholder.appendChild(headerText)

    headerText.setAttribute('class', 'lead');
    row.setAttribute('class', 'row');

    var name = document.createElement('input')
    name.setAttribute('type', 'text')
    name.setAttribute('class', 'form-control')
    name.setAttribute('placeholder', 'First name')
    formGroup.setAttribute('class', 'form-group')
    nameCol.setAttribute('class', 'col-6')
    placeholder.appendChild(name)

    var lastname = document.createElement('input')
    lastname.setAttribute('type', 'text')
    lastname.setAttribute('class', 'form-control')
    lastname.setAttribute('placeholder', 'Last name')
    formGroup2.setAttribute('class', 'form-group')
    lastNameCol.setAttribute('class', 'col-6')
    placeholder.appendChild(lastname)

    var email = document.createElement('input')
    email.setAttribute('type', 'text')
    email.setAttribute('class', 'form-control')
    email.setAttribute('placeholder', 'Email')
    formGroup3.setAttribute('class', 'form-group')
    emailCol.setAttribute('class', 'col')
    placeholder.appendChild(email)

    form.appendChild(row)
    row.appendChild(colSeperate);
    row.appendChild(nameCol)
    formGroup.appendChild(name)
    nameCol.appendChild(formGroup)

    row.appendChild(lastNameCol);
    lastNameCol.appendChild(formGroup2);
    formGroup2.appendChild(lastname);

    row.appendChild(emailCol);
    emailCol.appendChild(formGroup3);
    formGroup3.appendChild(email);

    placeHolder.appendChild(form);

}