function createTable() {
  var placeholder = document.getElementsByClassName('container')[0];

  var table = document.createElement('table');
  var thead = document.createElement('thead');
  var tbody = document.createElement('tbody');

  table.setAttribute('class', 'table table-striped');

  var headRow = document.createElement('tr');
  for (var i = 0; i < 4; i++) {
    var head = document.createElement('th');
    head.setAttribute('scope', 'col');
    headRow.appendChild(head);
  }
  headRow.childNodes[0].innerHTML = "#";
  headRow.childNodes[1].innerHTML = "First";
  headRow.childNodes[2].innerHTML = "Last";
  headRow.childNodes[3].innerHTML = "Handle";

  for (var i = 0; i < 3; i++) {
    var bodyRow = document.createElement('tr');
    var head = document.createElement('th');
    head.setAttribute('scope', 'row');
    bodyRow.appendChild(head);
    for (let i = 0; i < 3; i++) {
      bodyRow.appendChild(document.createElement('td'));
    }
    tbody.appendChild(bodyRow);
  }
  
  tbody.childNodes[0].childNodes[0].innerHTML = "1";
  tbody.childNodes[0].childNodes[1].innerHTML = "Mark";
  tbody.childNodes[0].childNodes[2].innerHTML = "Otto";
  tbody.childNodes[0].childNodes[3].innerHTML = "@mdo";

  tbody.childNodes[1].childNodes[0].innerHTML = "2";
  tbody.childNodes[1].childNodes[1].innerHTML = "Jacob";
  tbody.childNodes[1].childNodes[2].innerHTML = "Thornton";
  tbody.childNodes[1].childNodes[3].innerHTML = "@fat";

  tbody.childNodes[2].childNodes[0].innerHTML = "3";
  tbody.childNodes[2].childNodes[1].innerHTML = "Larry";
  tbody.childNodes[2].childNodes[2].innerHTML = "the Bird";
  tbody.childNodes[2].childNodes[3].innerHTML = "@twitter";

  table.appendChild(thead);
  thead.appendChild(headRow);
  table.appendChild(tbody);
  placeholder.appendChild(table);
}