function listFormElement() {
    var formElement = document.getElementsByTagName('form');
    console.log(formElement)
}

function copyTextFormNameToLastName() {
    var nameElement = document.getElementById('firstName')
    var surnameElement = document.getElementById('lastName')
    var firstName = nameElement.value
    console.log('firstname is ' + firstName)
    console.log('now lastname is ' + surnameElement.value)
    surnameElement.value = firstName
}

function resetForm() {
    var text = document.getElementsByTagName('form');
    for (var i = 0; i < text[0].length; i++) {
        text[0][i].value = null;
    }
}

function radioValue(inlineRadioOptions) {
    var text = document.getElementsByTagName('form');
    for (var i = 0; i < text[0].length; i++) {
        text[0][2].value = document.getElementById("email").value = inlineRadioOptions;
    }
}

function showSummary() {
    var emptyAll = true
    var input = document.getElementsByName('text')
    var summaryElement = document.getElementById('summary')
    for (var i = 0; i < input.length; i++) {
        if (input[i].value != "") {
            emptyAll = false
            break
        }
    }

    if (!emptyAll) {
        var name = document.getElementById('firstname').value
        var lastname = document.getElementById('lastname').value
        var email = document.getElementById('email').value
        var password = document.getElementById('exampleInputPassword1').value
        var confirmPass = document.getElementById('exampleInputPassword2').value
        var status = document.querySelector('input[name="inlineRadioOptions"]:checked').value
        var summaryElement = document.getElementById('summary')

        summaryElement.innerHTML = "First name: " + name
        summaryElement.innerHTML = summaryElement.innerHTML + "<br>Last name: " + lastname
        summaryElement.innerHTML = summaryElement.innerHTML + "<br>Email: " + email
        summaryElement.innerHTML = summaryElement.innerHTML + "<br>Password: " + password
        summaryElement.innerHTML = summaryElement.innerHTML + "<br>Confirm password: " + confirmPass
        summaryElement.innerHTML = summaryElement.innerHTML + "<br>Student status: " + status
        summaryElement.removeAttribute('hidden')
    } else {
        summaryElement.setAttribute('hidden', 'true')
    }
}
